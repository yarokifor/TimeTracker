FROM python:3

ENV PYTHONUNBUFFERED 1
RUN mkdir /opt/TimeTracker
WORKDIR /opt/TimeTracker
ADD hours/*py /opt/TimeTracker/hours/
ADD shifts/*py /opt/TimeTracker/shifts/
ADD shifts/templates/* /opt/TimeTracker/shifts/templates/
ADD shifts/migrations/* /opt/TimeTracker/shifts/migrations/
ADD shifts/management/* /opt/TimeTracker/shifts/management/
ADD manage.py /opt/TimeTracker
ADD requirements.txt /opt/TimeTracker
ADD docker_files/uwsgi.ini /etc/uwsgi.ini
ADD docker_files/local_settings.py /opt/TimeTracker/hours

RUN pip install -r requirements.txt
RUN pip install uwsgi

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/uwsgi", "/etc/uwsgi.ini"]


